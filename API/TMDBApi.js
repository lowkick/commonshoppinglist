const API_KEY = 'cb07257d77d591357aef10788663e9e1'

export function getApiMovies (text, page) {
    const url = 'https://api.themoviedb.org/3/search/movie?api_key=' + API_KEY + '&language=fr&query=' + text + '&page=' + page
    return fetch(url)
      .then((response) => response.json())
      .catch((error) => console.error(error))
  }
  
export function getImageFromApi (name) {
    return 'https://image.tmdb.org/t/p/w300' + name
  }

  export function getApiMovie(id) {
      const url = 'https://api.themoviedb.org/3/movie/' + id + '?api_key=' + API_KEY + '&language=fr'

      return fetch(url)
          .then((response) => response.json())
          .catch((error) => console.error(error))
  }