import {createStackNavigator } from 'react-navigation'
import Search from '../Components/Search'
import FilmDetail from '../Components/FilmDetail'

const SearchStackNavigation = createStackNavigator({
    Search: {
        screen: Search,
        navigationOptions: {
            title: 'Recherche'
        }
    },
    FilmDetail: {
        screen: FilmDetail
    }
})

export default SearchStackNavigation