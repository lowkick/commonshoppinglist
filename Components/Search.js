import React from "react";
import { View, Button, TextInput, FlatList, StyleSheet, ActivityIndicator } from "react-native";
import FilmItem from "./FilmItem";
import { getApiMovies  } from '../API/TMDBApi';
//import FilmItem from "./FilmItem";

class Search extends React.Component {

    constructor(props) {
        super(props);
        this.state = { 
            films: [], 
            isLoading: false
        }
        this.text = ''
        this.page = 0
        this.totalPages = 0
    }

    //On load l'api
    _loadMovies() {
        this.setState({isLoading: true})
        getApiMovies(this.text, this.page+1).then(data =>  {
            this.page = data.page;
            this.totalPages = data.total_pages
            this.setState({
                films: this.state.films.concat(data.results), 
                isLoading: false
            })
        })
    }
    _getText(text) {
        this.text = text
    }

    _displayLoader()
    {
        if (this.state.isLoading) {
            return <ActivityIndicator size='large' style={styles.loader} />
        }
        
    }

    //A chaque fois qu'on lance un recherche, on reinitialise la page et la liste des films avant de loader l'api
    _searchMovies()
    {
        this.page = 0
        this.totalPages = 0
        this.setState({films : []}, () => {
            this._loadMovies()
        })
    }

    _displayDetail = (id) => {
        this.props.navigation.navigate("FilmDetail", {id: id})
    }

    render() {
        return (
            <View>
                <TextInput 
                style={styles.textinput} 
                placeholder='Titre du film' 
                onChangeText={(text) => this._getText(text)} 
                onSubmitEditing={() => {this._searchMovies()}} />
                <Button title='Search' onPress={() => this._searchMovies()} />
                <FlatList 
                data={this.state.films}
                keyExtractor={(item) => item.id.toString() } 
                renderItem={({item}) => <FilmItem film={item} displayDetail={this._displayDetail} />}
                onEndReachedThreshold={0.5}
                onEndReached={() => {
                    if (this.state.films.length > 0 && this.page < this.totalPages) {
                        this._loadMovies()
                    }
                }}
                />
                {this._displayLoader()}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    textinput: {
        padding:5,
        marginTop:20,
        marginBottom: 10 
    },
    loader: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 100,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center'
    }
})
export default Search