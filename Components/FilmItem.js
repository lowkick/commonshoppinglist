import React from 'react';
import {View, StyleSheet, Image, Text, TouchableOpacity} from 'react-native';
import { getImageFromApi  } from '../API/TMDBApi';

class FilmItem extends React.Component {
    

    render() {
        const film = this.props.film
        const displayDetail = this.props.displayDetail

        return (
            <TouchableOpacity style={styles.main_container} 
            onPress={() => displayDetail(film.id)}
            >
                <Image style={styles.cover} source={{uri: getImageFromApi(film.poster_path) }} />
                <View style={styles.content_container}>
                    <View style={ styles.header_container}>
                        <Text style={styles.title}>{film.original_title}</Text>
                        <Text style={styles.rating}>{film.vote_average}</Text>
                    </View>
                    <View style={styles.description_container}>
                        <Text numberOfLines={6}>{film.overview}</Text>
                    </View>
                    <View style={styles.footer_container}>
                        <Text>{film.release_date}</Text>
                    </View>
                </View>  
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    main_container: {
        flexDirection: 'row',
        height:190
    },
    cover: {
        width: 120,
        height: 180,
        margin: 5,
        backgroundColor: 'gray'
    },
    content_container: {
        flex: 1,
        margin:5
    },
    header_container: {
        flexDirection: 'row',
        flex: 2
    },
    title: {
        flex:1,
        fontWeight: 'bold',
        fontSize:16,
        color: 'red',
        flexWrap:'wrap'
    },
    rating: {
        fontWeight: 'bold',
        fontSize: 20
    },
    description_container: {
        flex:8
    },
    footer_container: {
        flex:1
    }
})
export default FilmItem