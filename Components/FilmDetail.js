// Components/FilmDetail.js

import React from 'react'
import { StyleSheet, View, ScrollView, ActivityIndicator, Text } from 'react-native'
import { getApiMovie } from '../API/TMDBApi'

class FilmDetail extends React.Component {

    constructor(props) {
        super(props)
        console.log(this.props.navigation.state.params.id)
        console.log(this.props.navigation.getParam('id'))
        this.state = {
            film: undefined,
            isLoading: true
        }
    }

    _displayLoading() {
        if (this.state.isLoading) {
            // Si isLoading vaut true, on affiche le chargement à l'écran
            return (
                <View style={styles.loading_container}>
                    <ActivityIndicator size='large' />
                </View>
            )
        }
    }

    componentDidMount() {
        getApiMovie(this.props.navigation.getParam('id')).then(data => {
            this.setState({
                film: data,
                isLoading: false
            })
        })
    }

    display() {
        console.log(this.state.film)
        if(this.state.film != undefined) {
            return (
                <ScrollView style={styles.scrollview_container}>
                    <Text>{this.state.film.title}</Text>
                </ScrollView>
            )
        }
    }

    render() {
        return (
            <View style={styles.main_container}>
                {this._displayLoading()}
                {this.display()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    main_container: {
        flex: 1
    },
    loading_container: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center'
    },
    scrollview_container: {
        flex: 1
    }
})

export default FilmDetail